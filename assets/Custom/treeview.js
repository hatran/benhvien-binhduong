var Treeview = {
 init: function() {
  $("#m_tree_1").jstree({
   core: {
    themes: {
     responsive: !1
    }
   },
   types: {
    default: {
     icon: "fa fa-folder"
    },
    file: {
     icon: "fa fa-file"
    }
   },
   plugins: ["types"]
  }), $("#m_tree_2").jstree({
   core: {
    themes: {
     responsive: !1
    }
   },
   types: {
    default: {
     icon: "fa fa-folder m--font-warning"
    },
    file: {
     icon: "fa fa-file  m--font-warning"
    }
   },
   plugins: ["types"]
  }), $("#m_tree_2").on("select_node.jstree", function(e, t) {
   var n = $("#" + t.selected).find("a");
   if ("#" != n.attr("href") && "javascript:;" != n.attr("href") && "" != n.attr("href")) return "_blank" == n.attr("target") && (n.attr("href").target = "_blank"), document.location.href = n.attr("href"), !1
  }), $("#m_tree_3").jstree({
   plugins: ["wholerow", "checkbox", "types"],
   core: {
    themes: {
     responsive: !1
    },
    data: [{
     text: "Dự thảo luật kinh tế",
     children: [{
      text: "Dự thảo luật thuế sửa đổi",
      state: {
       selected: !0
      }
     }, {
      text: "Luật đầu tư công",
      icon: "fa fa-warning m--font-danger"
     }, {
      text: "Luật doanh nghiệp",
      icon: "fa fa-folder m--font-default",
      state: {
       opened: !0
      },
      children: ["Dự thảo"]
     }, {
      text: "Luật Đơn vị hành chính kinh tế đặc biệt",
      icon: "fa fa-warning m--font-waring"
     }, {
      text: "Luật thuế tài sản",
      icon: "fa fa-check m--font-success",
      state: {
       disabled: !0
      }
     }]
    }, "Luật cạnh tranh sửa đổi"]
   },
   types: {
    default: {
     icon: "fa fa-folder m--font-warning"
    },
    file: {
     icon: "fa fa-file  m--font-warning"
    }
   },  plugins: ["contextmenu", "state", "types","dnd"]
  }), $("#m_tree_4").jstree({
   core: {
    themes: {
     responsive: !1
    },
    check_callback: !0,
    data: [{
     text: "Báo cáo giám sát",
     children: [{
      text: "Giám sát cổ phần hóa DN nhà nước",
      state: {
       selected: !0
      }
     }, {
      text: "Quản lý và sử dụng vốn",
      icon: "fa fa-warning m--font-danger"
     }, {
      text: "Tài sản",
      icon: "fa fa-folder m--font-success",
      state: {
       opened: !0
      },
      children: [{
       text: "Giám sát tài sản công",
       icon: "fa fa-file m--font-waring"
      }]
     }, {
      text: "Báo cáo giám sát sử dụng vốn ngấn sách",
      icon: "fa fa-warning m--font-waring"
     }, {
      text: "Giám sát chi ngân sách",
      icon: "fa fa-check m--font-success",
      state: {
       disabled: !0
      }
     }, {
      text: "Giám sát thu ngân sách",
      icon: "fa fa-folder m--font-danger",
      children: [{
       text: "Năm 2018",
       icon: "fa fa-file m--font-waring"
      }, {
       text: "Năm 2017",
       icon: "fa fa-file m--font-success"
      }, {
       text: "Năm 2016",
       icon: "fa fa-file m--font-default"
      }, {
       text: "Năm 2015",
       icon: "fa fa-file m--font-danger"
      }, {
       text: "Năm 2014",
       icon: "fa fa-file m--font-info"
      }]
     }]
    }, "Các báo cáo khác"]
   },
   types: {
    default: {
     icon: "fa fa-folder m--font-brand"
    },
    file: {
     icon: "fa fa-file  m--font-brand"
    }
   },
   state: {
    key: "demo2"
   },
   plugins: ["contextmenu", "state", "types"]
  }), 
  
  
  $("#m_tree_demo").jstree({
   core: {
    themes: {
     responsive: !1
    },
    check_callback: !0,
    data: [{
     text: "Các tài liệu luật kinh tế",
     children: [{
      text: "Tài liệu luật doanh nghiệp sửa đổi",
      state: {
       selected: !0
      }
     }, {
      text: "Tài liệu luật đầu tư công",
      icon: "fa fa-warning m--font-danger"
     }, {
      text: "Tài liệu luật cạnh tranh",
      icon: "fa fa-folder m--font-success",
      state: {
       opened: !0
      },
      children: [{
       text: "Tài liệu luật cổ phần hóa",
       icon: "fa fa-file m--font-waring"
      }]
     }, {
      text: "Tài liệu tài sản công",
      icon: "fa fa-warning m--font-waring"
     }, {
      text: "Tài liệu quản lý vốn",
      icon: "fa fa-check m--font-success",
      state: {
       disabled: !0
      }
     }, {
      text: "Tài liệu dự thảo luật",
      icon: "fa fa-folder m--font-danger",
      children: [{
       text: "Tìa liệu dự thảo mới",
       icon: "fa fa-file m--font-waring"
      }, {
       text: "Tài liệu đã thông qua",
       icon: "fa fa-file m--font-success"
      }, {
       text: "Tài liệu chưa thông qua",
       icon: "fa fa-file m--font-default"
      }, {
       text: "Tìa liệu cần theo dõi",
       icon: "fa fa-file m--font-danger"
      }, {
       text: "Tài liệu cần bổ sung",
       icon: "fa fa-file m--font-info"
      }]
     }]
    }, "Các tài liệu khác liên quan dự thảo"]
   },
   types: {
    default: {
     icon: "fa fa-folder m--font-brand"
    },
    file: {
     icon: "fa fa-file  m--font-brand"
    }
   },
   state: {
    key: "demo2"
   },
   plugins: ["contextmenu", "state", "types","dnd"]
  })
 }
};
jQuery(document).ready(function() {
 Treeview.init()
});