var mook = {
    sobn: function () {
        let raw = `
Sở ban ngành
Huyện Ba Chẽ
Huyện Cô Tô
Huyện Hải Hà
Huyện Tiên Yên
Tp. Cẩm Phả
TP.Uông Bí
TX.Đông Triều
Huyện Bình Liêu
Huyện Đầm Hà
Huyện Hoành Bồ
Huyện Vân Đồn
TP.Hạ Long 
TP.Móng Cái
TX.Quảng Yên
Sở Công thương
Sở GDDT
Sở Kế hoạch và đầu tư`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
TP.Móng Cái
TX.Quảng Yên
Sở Công thương
Sở GDDT
Sở Kế hoạch và đầu tư
Sở LD TBXH
Sở Nông nghiệp và PTNT
Sở Tài chính
Sở Tư pháp
Sở Văn hóa thể thao
Sở Du lịch
Sở Giao thông vận tải
Sở Khoa học và Công nghệ
Sở Nội vụ
Sở Ngoại vụ
Sở TNMT
Sở TTTT
Sở Xây dựng
Sở Y tế
Công an tỉnh
Cục Hải Quan tỉnh
Liên minh hợp tác xã tỉnh
`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `
Huyện Hoành Bồ
Huyện Vân Đồn
TP.Hạ Long 
TP.Móng Cái
TX.Quảng Yên
Sở Công thương
Sở GDDT
Sở Kế hoạch và đầu tư
Sở LD TBXH
Sở Nông nghiệp và PTNT
Sở Tài chính
Sở Tư pháp
Sở Văn hóa thể thao
Sở Du lịch
Sở Giao thông vận tải
Sở Khoa học và Công nghệ
Sở Nội vụ
Sở Ngoại vụ
Sở TNMT
Sở TTTT
Sở Xây dựng
Sở Y tế
Công an tỉnh
Cục Hải Quan tỉnh
Liên minh hợp tác xã tỉnh
`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
TX.Đông Triều
Huyện Bình Liêu
Huyện Đầm Hà
Huyện Hoành Bồ
Huyện Vân Đồn
TP.Hạ Long 
TP.Móng Cái
TX.Quảng Yên
Sở Công thương
Sở GDDT
Sở Kế hoạch và đầu tư
Sở LD TBXH
Sở Nông nghiệp và PTNT
Sở Tài chính
Sở Tư pháp
Sở Văn hóa thể thao
Sở Du lịch
Sở Giao thông vận tải
Sở Khoa học và Công nghệ
Sở Nội vụ
Sở Ngoại vụ
Sở TNMT
Sở TTTT
Sở Xây dựng
Sở Y tế
Công an tỉnh
Cục Hải Quan tỉnh
Liên minh hợp tác xã tỉnh

`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
Sở ban ngành
Huyện Ba Chẽ
Huyện Cô Tô
Huyện Hải Hà
Huyện Tiên Yên
Tp. Cẩm Phả
TP.Uông Bí
TX.Đông Triều
Huyện Bình Liêu
Huyện Đầm Hà
Huyện Hoành Bồ
Huyện Vân Đồn
TP.Hạ Long 
TP.Móng Cái
TX.Quảng Yên
Sở Công thương
Sở GDDT
Sở Kế hoạch và đầu tư
Sở LD TBXH
Sở Nông nghiệp và PTNT
Sở Tài chính
Sở Tư pháp
Sở Văn hóa thể thao
`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
