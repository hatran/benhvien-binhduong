var dataphongkham = [
 {
   "STT": 1,
   "Name": "Công Ty Cổ Phần Bệnh Viện Đa Khoa An Phú",
   "address": "Số 5, KDC. Nam Phương, Đ. 22/12 Khu Phố 1A, P. An Phú, Thị xã Thuận An, Bình Dương",
   "Longtitude": 10.9429736,
   "Latitude": 106.7474271
 },
 {
   "STT": 2,
   "Name": "Phòng Khám Đa Khoa Thủ Dầu Một",
   "address": "303 ĐL Bình Dương, P.Chánh Nghĩa, Thành phốThủ Dầu Một, Bình Dương",
   "Longtitude": 10.9659982,
   "Latitude": 106.6689991
 },
 {
   "STT": 3,
   "Name": "Công Ty TNHH Phòng Khám Đa Khoa Hưởng Phúc",
   "address": "1/424 Đường Thuận Giao 21, Khu phố Hòa Lân 2, Thuận Giao, Thuận An, Bình Dương",
   "Longtitude": 10.9491763,
   "Latitude": 106.7211163
 },
 {
   "STT": 4,
   "Name": "Phòng Khám Ngoài Giờ",
   "address": "46/12 An Nhơn, Khu phố Tân Phú 1, P. Tân Bình, Thị xã Dĩ An, Bình Dương",
   "Longtitude": 10.9297241,
   "Latitude": 106.7620376
 },
 {
   "STT": 5,
   "Name": "Phòng khám Đa khoa An Phước Sài Gòn",
   "address": "Đường tỉnh 747B, khu phố Khánh Lộc, Khánh Bình, Tân Uyên, Bình Dương",
   "Longtitude": 11.0375325,
   "Latitude": 106.7589352
 },
 {
   "STT": 6,
   "Name": "Phòng khám Đa khoa Nhân Nghĩa",
   "address": "99 quốc lộ 13, khu phố 2, Mỹ Phước, Bến Cát, Bình Dương",
   "Longtitude": 11.1331975,
   "Latitude": 106.6059154
 },
 {
   "STT": 7,
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt Sài Gòn MT",
   "address": "34 khu phố Khánh Hòa, Tân Phước Khánh, Tân Uyên, Bình Dương",
   "Longtitude": 11.0026673,
   "Latitude": 106.7322637
 },
 {
   "STT": 8,
   "Name": "Phòng khám Đa khoa Tâm Thiện Tâm",
   "address": "T6/45M, khu phố Bình Thuận 2, Thuận Giao, Thuận An, Bình Dương",
   "Longtitude": 10.96379,
   "Latitude": 106.714325
 },
 {
   "STT": 9,
   "Name": "Phòng khám Mắt - Bác sĩ Huỳnh Trần Dương Giang",
   "address": "25B4 Hoàng Văn Thụ, Chánh Nghĩa, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9763618,
   "Latitude": 106.664914
 },
 {
   "STT": 10,
   "Name": "Nha khoa Bình Dương",
   "address": "494-496 đại lộ Bình Dương, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9895788,
   "Latitude": 106.6640514
 },
 {
   "STT": 11,
   "Name": "Phòng khám Ngoại và Nam khoa - Bác sĩ Dương Thế Anh",
   "address": "121/28 Phạm Ngọc Thạch, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9955148,
   "Latitude": 106.6565124
 },
 {
   "STT": 12,
   "Name": "Phòng khám Phụ sản Chiêu Liêu",
   "address": "45/16A Lê Hồng Phong, Tân Đông Hiệp, Dĩ An, Bình Dương",
   "Longtitude": 10.9166532,
   "Latitude": 106.7685971
 },
 {
   "STT": 13,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Nguyễn Thị Thắm",
   "address": "506, khu phố 2, Mỹ Phước, Bến Cát, Bình Dương",
   "Longtitude": 11.1316421,
   "Latitude": 106.613399
 },
 {
   "STT": 14,
   "Name": "Nha khoa Toàn Mỹ",
   "address": "Nguyễn An Ninh, Dĩ An, Dĩ An, Bình Dương",
   "Longtitude": 10.9041928,
   "Latitude": 106.7681538
 },
 {
   "STT": 15,
   "Name": "Phòng khám Đa khoa Vạn Phúc 1",
   "address": "B19 Lô H11 & H12 Đường Trần Quốc Toản, Hòa Phú, Thủ Dầu Một, Bình Dương",
   "Longtitude": 11.0708921,
   "Latitude": 106.6749468
 },
 {
   "STT": 16,
   "Name": "Phòng khám Đa khoa Bạch Đằng",
   "address": "102 Ngô Quyền, Phú Cường, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9814973,
   "Latitude": 106.6518175
 },
 {
   "STT": 17,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Huỳnh Thị Ngọc Mai",
   "address": "12 Phan Đình Phùng, khu phố Chợ, Lái Thiêu, Thuận An, Bình Dương",
   "Longtitude": 10.9041352,
   "Latitude": 106.6996315
 },
 {
   "STT": 18,
   "Name": "Phòng khám Nội - Nhi tổng quát - Bác sĩ Trần Thị Minh Nguyệt",
   "address": "881 Lê Hồng Phong, Phú Thọ, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9625957,
   "Latitude": 106.6765755
 },
 {
   "STT": 19,
   "Name": "Phòng khám Phụ khoa - Bác sĩ Nguyễn Thị Nương",
   "address": "Đường tỉnh 747, Uyên Hưng, Tân Uyên, Bình Dương",
   "Longtitude": 11.0791185,
   "Latitude": 106.7837461
 },
 {
   "STT": 20,
   "Name": "Phòng khám Đa khoa Vũ Cao",
   "address": "20/22 Đường Mồi, Dĩ An, Dĩ An, Bình Dương",
   "Longtitude": 10.9036627,
   "Latitude": 106.7557865
 },
 {
   "STT": 21,
   "Name": "Nha khoa Nissei",
   "address": "Lầu 2, trung tâm thương mại Minh Sang, Thuận An, Thuận An, Bình Dương",
   "Longtitude": 10.9359969,
   "Latitude": 106.7100988
 },
 {
   "STT": 22,
   "Name": "Phòng khám Y khoa Sài Gòn",
   "address": "10/21A Trần Hưng Đạo, tổ 21, khu phố Bình Minh 1, Dĩ An, Dĩ An, Bình Dương ",
   "Longtitude": 10.8984899,
   "Latitude": 106.7816859
 },
 {
   "STT": 23,
   "Name": "Phòng khám Đa khoa Hưởng Phúc 115",
   "address": "1/424 khu phố Hòa Lan 2, Thuận Giao, Bình Dương",
   "Longtitude": 10.9626068,
   "Latitude": 106.7008099
 },
 {
   "STT": 24,
   "Name": "Nha khoa Đại Nam - Cơ sở 6",
   "address": "584 Cách Mạng Tháng Tám, Phú Cường, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9762747,
   "Latitude": 106.6576197
 },
 {
   "STT": 25,
   "Name": "Phòng khám Nhi Đồng - Việt Sing ",
   "address": "82/1 khu phố 1A, ĐT 743 An Phú, Thuận An, Bình Dương",
   "Longtitude": 10.9464798,
   "Latitude": 106.742
 },
 {
   "STT": 26,
   "Name": "Nha khoa Linh Xuân 2",
   "address": "23/10 Quốc lộ 1 K khu phố Tân Hòa, Đông Hòa, Dĩ An, Bình Dương",
   "Longtitude": 10.8914048,
   "Latitude": 106.7881661
 },
 {
   "STT": 27,
   "Name": "Phòng khám Đa khoa Vạn Phúc 2",
   "address": "8 Nguyễn Văn Tiết, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9923142,
   "Latitude": 106.6557214
 },
 {
   "STT": 28,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Dương Thanh Hiền",
   "address": "257 Hoàng Hoa Thám, khu 5, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.998216,
   "Latitude": 106.6677762
 },
 {
   "STT": 29,
   "Name": "Phòng khám Nhi khoa - Bác sĩ Phạm Văn Lương",
   "address": "69 âp Hòa cường, Minh Hòa, Dầu Tiếng, Bình Dương",
   "Longtitude": 11.459618,
   "Latitude": 106.4434433
 },
 {
   "STT": 30,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Lưu Hoàng Đoàn Thị Cẩm Tú",
   "address": "543 Phạm Ngọc Thạch, Phú Mỹ, Thủ Dầu Một, Bình Dương",
   "Longtitude": 11.0108757,
   "Latitude": 106.6720676
 },
 {
   "STT": 31,
   "Name": "Phòng khám Đa khoa Y Dược An Sài Gòn",
   "address": "57B quốc lộ 1K, Nội Hoá, Bình An, Dĩ An, Bình Dương",
   "Longtitude": 10.8987637,
   "Latitude": 106.7939051
 },
 {
   "STT": 32,
   "Name": "Phòng khám Nha khoa An Bình 2",
   "address": "29B/16 Bình Đáng, Bình Hòa, Thuận An, Bình Dương",
   "Longtitude": 10.9175508,
   "Latitude": 106.7341765
 },
 {
   "STT": 33,
   "Name": "Nha khoa Quốc Tế Thủ Dầu Một",
   "address": "329 Đại Lộ Bình Dương, Chánh Nghĩa, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9759513,
   "Latitude": 106.6693979
 },
 {
   "STT": 34,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Trương Thị Trinh",
   "address": "04A Lô II, Bình Hòa, Lái Thiêu, Thuận An, Bình Dương",
   "Longtitude": 10.9184557,
   "Latitude": 106.7100713
 },
 {
   "STT": 35,
   "Name": "Nha khoa Đại Nam - Cơ sở 8",
   "address": "33/A, Uyên Hương, Tân Uyên, Bình Dương",
   "Longtitude": 11.0841596,
   "Latitude": 106.7883416
 },
 {
   "STT": 36,
   "Name": "Phòng khám chuyên khoa Phụ Sản - Kế hoạch hóa gia đình - Siêu âm Tân Phú",
   "address": "1/26 Bình Đường 4, An Bình, Dĩ An, Bình Dương",
   "Longtitude": 10.8867288,
   "Latitude": 106.7577247
 },
 {
   "STT": 37,
   "Name": "Nha khoa Đại Nam - Cơ sở 7",
   "address": "180 Yersin, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9824542,
   "Latitude": 106.6587778
 },
 {
   "STT": 38,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Nguyễn Thị Nương",
   "address": "001C tỉnh lộ 747, khu phố 3, Uyên Hưng, Tân Uyên, Bình Dương",
   "Longtitude": 11.0791185,
   "Latitude": 106.7837461
 },
 {
   "STT": 39,
   "Name": "Phòng khám Ngoại tổng hợp - Bác sĩ Hàn Khởi Quang",
   "address": "190 Thích Quảng Đức, Phú Cường, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9771263,
   "Latitude": 106.6640264
 },
 {
   "STT": 40,
   "Name": "Phòng khám Đa khoa Đại Minh Phước",
   "address": "41, đường 11, khu công nghiệp Mỹ Phước 1, Mỹ Phước, Bến Cát, Bình Dương",
   "Longtitude": 11.1289604,
   "Latitude": 106.5910077
 },
 {
   "STT": 41,
   "Name": "Phòng khám Vinh Anh Sài Gòn",
   "address": "14 Nguyễn Trãi, Đường Mồi, Dĩ An, Dĩ An, Bình Dương",
   "Longtitude": 10.9101266,
   "Latitude": 106.7601428
 },
 {
   "STT": 42,
   "Name": "Nha khoa Sài Gòn 5",
   "address": "251C/1 khu phố 1A, An Phú, Thuận An, Bình Dương",
   "Longtitude": 10.8748849,
   "Latitude": 106.6986826
 },
 {
   "STT": 43,
   "Name": "Phòng khám Nội Tổng hợp - Bác sĩ Nguyễn Đắc Thắng",
   "address": "Đường tỉnh 746, ấp Long Bình, Khánh Bình, Tân Uyên, Bình Dương",
   "Longtitude": 11.0492928,
   "Latitude": 106.7556566
 },
 {
   "STT": 44,
   "Name": "Phòng khám Đa khoa Phúc Tâm Phúc",
   "address": "Hội Nghĩa, Tân Uyên, Tân Uyên, Bình Dương",
   "Longtitude": 11.1005706,
   "Latitude": 106.767069
 },
 {
   "STT": 45,
   "Name": "Phòng khám chuyên khoa Nội Nhi - Bác sĩ Nguyễn Văn Nhưỡng",
   "address": "A40 tổ 13, khu 1, Hoàng Hoa Thám, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9874475,
   "Latitude": 106.6652318
 },
 {
   "STT": 46,
   "Name": "Phòng khám Đa khoa Phúc An Khang",
   "address": "1/4 Đại lộ Bình Dương, khu phố Hòa Lân 1, Thuận Giao, Thuận An, Bình Dương",
   "Longtitude": 10.9510691,
   "Latitude": 106.7055072
 },
 {
   "STT": 47,
   "Name": "Phòng khám Sản phụ khoa - Kế hoạch hoá gia đình - Bác sĩ Trương Thị Kim Hoàn",
   "address": "403 Cách Mạng Tháng Tám, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9848474,
   "Latitude": 106.6526655
 },
 {
   "STT": 48,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ Nguyễn Văn Hiển",
   "address": "112 Lò Chén, Chánh Nghĩa, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9699168,
   "Latitude": 106.6565371
 },
 {
   "STT": 49,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Nguyễn Thị Diễm Thi",
   "address": "8/3A, Bình Giao, Thuận Giao,, Thuận An, Bình Dương",
   "Longtitude": 10.9323736,
   "Latitude": 106.7117655
 },
 {
   "STT": 50,
   "Name": "Phòng khám Nội Nhi tổng quát - Bác sĩ Nguyễn Hồng Chương",
   "address": "446 Cách Mạng Tháng 8, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9826714,
   "Latitude": 106.6528715
 },
 {
   "STT": 51,
   "Name": "Phòng khám Tai Mũi Họng - Bác sĩ Nguyễn Tuấn Dũng",
   "address": "57 Nguyễn Văn Tiết, Hiệp Thành, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9910758,
   "Latitude": 106.6550247
 },
 {
   "STT": 52,
   "Name": "Nha khoa Sài Gòn 1",
   "address": "379 Cách Mạng Tháng Tám, Phú Cường, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9855202,
   "Latitude": 106.6524518
 },
 {
   "STT": 53,
   "Name": "Phòng khám Đa khoa Phúc Tâm",
   "address": "khu phố Ông Đông, Tân Hiệp, Tân Uyên, Bình Dương",
   "Longtitude": 11.0661153,
   "Latitude": 106.7367281
 },
 {
   "STT": 54,
   "Name": "Nha khoa Sài Gòn 4",
   "address": "162 Cách Mạng Tháng Tám, Chánh Mỹ, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.9925422,
   "Latitude": 106.6505828
 },
 {
   "STT": 55,
   "Name": "Phòng khám Sản phụ khoa - Bác sĩ Phan Thị Ngọc Lan",
   "address": "13A/C1 Khu phố 3, khu dân cư An Phú, An Phú, Thuận An, Bình Dương",
   "Longtitude": 10.9528008,
   "Latitude": 106.7278054
 },
 {
   "STT": 56,
   "Name": "Phòng khám Đa khoa Nhân Sinh",
   "address": "DC01, O6, khu dân cư Việt Sing, An Phú, Thuận An, Bình Dương",
   "Longtitude": 10.9400505,
   "Latitude": 106.7355929
 },
 {
   "STT": 57,
   "Name": "Phòng khám Đa khoa Sài Gòn Nhi",
   "address": "Đường tỉnh 743 khu phố Bình Đức 3, Bình Hòa, Thuận An, Bình Dương",
   "Longtitude": 10.9050243,
   "Latitude": 106.7263379
 },
 {
   "STT": 58,
   "Name": "Phòng Chẩn trị Đông y Phụ Tử Đường",
   "address": "8/21 Thủ Khoa Huân, ấp Bình Phú, Bình Chuẩn, Thuận An, Bình Dương",
   "Longtitude": 10.9752825,
   "Latitude": 106.7169535
 },
 {
   "STT": 59,
   "Name": "Phòng khám Da liễu - Bác sĩ Thu Thảo",
   "address": "1 Nguyễn An Ninh, An Bình, Dĩ An, Bình Dương",
   "Longtitude": 10.9037364,
   "Latitude": 106.7681616
 },
 {
   "STT": 60,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ Nguyễn Văn Thái",
   "address": "389 Nguyễn An Ninh, Dĩ An, Dĩ An, Bình Dương",
   "Longtitude": 10.9140581,
   "Latitude": 106.769427
 }
];