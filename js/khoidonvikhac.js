var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục An toàn vệ sinh thực phẩm             ",
   "icon": "maps/map-images/attp.png",
   "address": "209, Yersin, Phường Phú Cường, Thị Xã Thủ Dầu Một, Tỉnh Bình Dương, Phú Cường, Thủ Dầu Một, Bình Dương, Việt Nam",
   "Longtitude": 10.9818467,
   "Latitude": 106.6579385
 },
 {
   "STT": 2,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "icon": "maps/map-images/cckhhgd.png",
   "address": "106 Ba Mươi Tháng Tư, Phú Hoà, Tp. Thủ Dầu Một, Bình Dương, Việt Nam",
   "Longtitude": 10.977757,
   "Latitude": 106.6699239
 },
 {
   "STT": 3,
   "Name": "Trung tâm Bảo vệ Sức khoẻ lao động và Môi trường",
   "icon": "maps/map-images/ttytdp.png",
   "address": "497 Lê Hồng Phong, Phú Hoà, Thủ Dầu Một, Bình Dương, Việt Nam",
   "Longtitude": 10.9700014,
   "Latitude": 106.6805537
 },
 {
   "STT": 5,
   "Name": "Trung tâm Giám định Y khoa - Pháp y ",
   "icon": "maps/map-images/gdyk.png",
   "address": "278 Phạm Ngọc Thạch, Hiệp Thành, Thủ Dầu Một, Bình Dương, Việt Nam",
   "Longtitude": 10.99581,
   "Latitude": 106.6538261
 },
 {
   "STT": 7,
   "Name": "Trung tâm chăm sóc sức khỏe sinh sản Bình Dương",
   "icon": "maps/map-images/skss.png",
   "address": "213 Yersin, Thành phốThủ Dầu Một, Tỉnh Bình Dương",
   "Longtitude": 10.900708,
   "Latitude": 106.764903
 }
];