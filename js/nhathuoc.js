var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Nhà thuốc Xuân Trang",
   "address": "92 Văn Công Khai, P. Phú Cường, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.975988,
   "Latitude": 106.6530558
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc Dưỡng Ngương Đường",
   "address": "12 Đoàn Trần Nghiệp, P. Phú Cường, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9788405,
   "Latitude": 106.653181
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc 101",
   "address": "54/15 Hai Bà Trưng, Phú Cường, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9765472,
   "Latitude": 106.6516655
 },
 // {
 //   "STT": 4,
 //   "Name": "Nhà thuốc Thùy Hương",
 //   "address": "20/2 Lê Hồng Phong, P. Phú Hòa, thành phố Thủ Dầu Một, tỉnh Bình Dương",
 //   "Longtitude": 10.7649862,
 //   "Latitude": 106.6754201
 // },
 {
   "STT": 5,
   "Name": "Nhà thuốc Minh Thư",
   "address": "239 Lê Hồng Phong, P. Phú Hòa, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9823188,
   "Latitude": 106.6803343
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Ích Sanh Đường",
   "address": "1173/13 Chợ Bưng Cầu, P. Hiệp An, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0248901,
   "Latitude": 106.6468875
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Bá Lộc",
   "address": "183, Lý Thường Kiệt, P. Chánh Nghĩa, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9771021,
   "Latitude": 106.6524609
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Phương My",
   "address": "253 Hồ Văn Cống, P. Tương Bình Hiệp, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0079157,
   "Latitude": 106.6339116
 },
 {
   "STT": 9,
   "Name": "Nhà thuốc Hòa Phú",
   "address": "Chợ Hòa Phú, P. Hòa Phú, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0689943,
   "Latitude": 106.6703963
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Chị Hằng",
   "address": "D1 Khu Phố Hòa Phú, P. Hòa Phú, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0689943,
   "Latitude": 106.6703963
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc Bảo Ngọc",
   "address": "635 Phú Lợi, P. Phú Lợi, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9883316,
   "Latitude": 106.6928214
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc Phú Hoà",
   "address": "204 Lê Hồng Phong, P.Phú Hòa, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9835971,
   "Latitude": 106.6798831
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "117 Đường 30 Tháng 4, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9750113,
   "Latitude": 106.6718565
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc Mỹ Kiều",
   "address": "378 Đại Lộ Bình Dương, P. Phú Lợi, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9851796,
   "Latitude": 106.6661292
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc Hồng Nhiên",
   "address": "422 Đường CMT8, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9836084,
   "Latitude": 106.6526395
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc Hoàng Liêm",
   "address": "248 Yersin, P.Hiệp Thành, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9817803,
   "Latitude": 106.6574909
 },
 {
   "STT": 17,
   "Name": "Nhà thuốc HL",
   "address": "01 Hoàng Văn Thụ, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9771464,
   "Latitude": 106.6646904
 },
 {
   "STT": 18,
   "Name": "Nhà thuốc Phương My 2",
   "address": "Đường Hồ Văn Cống, p.Tương Bình Hiệp, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0042176,
   "Latitude": 106.6418142
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc số 20",
   "address": "Đường Cách Mạng tháng 8, p. Phú Cường, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9816092,
   "Latitude": 106.6539739
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Hòa Cảnh",
   "address": "Đường số 11 KP1 phường Hòa Phú, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 11.0913642,
   "Latitude": 106.6915186
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Bảo Ngọc",
   "address": "635 đường Phú Lợi, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9868003,
   "Latitude": 106.6927804
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc 89",
   "address": "Đường 30 tháng Tư, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9680224,
   "Latitude": 106.669585
 },
 {
   "STT": 23,
   "Name": "Quầy thuốc Huyền My",
   "address": "48/68 kp 22 Hoàng Hoa Thám, P. Phú Lợi, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9882576,
   "Latitude": 106.6655219
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Lê Hồng Phong",
   "address": "77 ngã tư địa chất đường Lê Hồng Phong, P. Phú Hòa, thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.9760566,
   "Latitude": 106.681432
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Phạm Giỏi",
   "address": "Ấp 1 xã Hưng Hòa, huyện Bàu Bàng,tỉnh Bình Dương",
   "Longtitude": 11.2207419,
   "Latitude": 106.6939803
 },
 {
   "STT": 26,
   "Name": "Quầy thuốc Hoàng Nghĩa",
   "address": "Chợ Bàu Bàng, Xã Lai Uyên, huyện Bàu Bàng,tỉnh Bình Dương",
   "Longtitude": 11.2436658,
   "Latitude": 106.6368628
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Chợ Bàu Bàng, Xã Lai Uyên, huyện Bàu Bàng,tỉnh Bình Dương",
   "Longtitude": 11.2436658,
   "Latitude": 106.6368628
 },
 {
   "STT": 28,
   "Name": "Quầy thuốc Phạm Giỏi",
   "address": "Ấp 3 xã Hưng Hòa, huyện Bàu Bàng,tỉnh Bình Dương",
   "Longtitude": 11.2207419,
   "Latitude": 106.6939803
 },
 {
   "STT": 29,
   "Name": "Quầy thuốc Y Đức",
   "address": "03 Tổ 3 Hùng Vương, TT Dầu Tiếng, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2824625,
   "Latitude": 106.3644522
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Bác Sĩ Chín",
   "address": "Ấp Hòa Cường, xã Minh Hòa, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.459618,
   "Latitude": 106.4434433
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Minh Thúy",
   "address": "Long Chiểu, xã Long Tân, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2725582,
   "Latitude": 106.550444
 },
 {
   "STT": 32,
   "Name": "Quầy thuốc Nguyễn Hồng",
   "address": "27/12 KP2, TT Dầu Tiếng, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2781002,
   "Latitude": 106.3620161
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Số 1 - Bệnh Viện Dầu Tiếng",
   "address": "KP 4B, TT Dầu Tiếng, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2672094,
   "Latitude": 106.3675416
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Thiên Kim",
   "address": "Ấp Chợ, xã Thanh Tuyền, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.1672027,
   "Latitude": 106.469449
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Nguyễn Hồng",
   "address": "7/12 khu phố 2 TT Dầu Tiếng, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2781002,
   "Latitude": 106.3620161
 },
 {
   "STT": 36,
   "Name": "Đông y Dưỡng Sanh Đường",
   "address": "chợ Sáng, TT Dầu Tiếng, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.2737538,
   "Latitude": 106.3571938
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Thiên Kim",
   "address": "ấp Chợ, xã Thanh Tuyền, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.1672027,
   "Latitude": 106.469449
 },
 {
   "STT": 38,
   "Name": "Nhà thuốc Minh Châu",
   "address": "ấp Chợ, xã Thanh Tuyền, huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.1672027,
   "Latitude": 106.469449
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc Thiện Đức Đường",
   "address": "220 Hùng Vương, P. Mỹ Phước, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1538063,
   "Latitude": 106.590747
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Hòa Dương",
   "address": "19 Hùng Vương, P. Mỹ Phước, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1521776,
   "Latitude": 106.589034
 },
 {
   "STT": 41,
   "Name": "Quầy thuốc Mộng Nghi",
   "address": "D9 KCN Mỹ Phước 1, P. Mỹ Phước, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.124821,
   "Latitude": 106.597897
 },
 {
   "STT": 42,
   "Name": "Quầy thuốc Lê Mộng Thúy",
   "address": "Ấp Lồ Ồ, An Tây, xã Phú An, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.087395,
   "Latitude": 106.548505
 },
 {
   "STT": 43,
   "Name": "Quầy thuốc 30",
   "address": "278/10 ĐT 741, KP1, P. Tân Định, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.0600602,
   "Latitude": 106.635025
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc Hoàng Yến",
   "address": "DB 8 Mỹ Phước 2 TX,Bến Cát, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1511308,
   "Latitude": 106.6077937
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc An Lợi",
   "address": "383 tổ 2 kp An Lợi, P. Hòa Lợi, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1241533,
   "Latitude": 106.6651767
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Kim Dung",
   "address": "Đường NJ 10, Mỹ Phước 3, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1457453,
   "Latitude": 106.6082396
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Ngọc Diệp",
   "address": "TC1 Mỹ Phước 2, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1231572,
   "Latitude": 106.611198
 },
 {
   "STT": 48,
   "Name": "Quầy thuốc 30",
   "address": "278/10 đường DT741 P. Tân Định, TX Bến Cát, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.0600602,
   "Latitude": 106.635025
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc Mười Thanh",
   "address": "D2 Mỹ Phước 1, Tx Bình Dương, thị Xã Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.1253821,
   "Latitude": 106.602971
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Phước Hòa",
   "address": "435A/Tổ 5, Ấp 1B, xã Phước Hòa, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.229034,
   "Latitude": 106.7302046
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Hạnh Hòa Đường",
   "address": "11 Độc Lập, KP2, TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2884956,
   "Latitude": 106.7930769
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Huê Linh Đường",
   "address": "64 KP2, TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2963709,
   "Latitude": 106.8060388
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Dũng Hạnh",
   "address": "04 KP Khánh Lợi, P. Tân Phước Khánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0026673,
   "Latitude": 106.7322637
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc Đức Tuấn",
   "address": "KP1, P. Uyên Hưng, TT Tân Uyên, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0841596,
   "Latitude": 106.7883416
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Số 199",
   "address": "Khu 3, Chợ Mới, P. Uyên Hưng, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0617057,
   "Latitude": 106.7951186
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc Thành Hiền",
   "address": "Âp 1, xã Hội Nghĩa, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2655822,
   "Latitude": 106.7706458
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Ngọc Nga",
   "address": "Chợ Tân Phước Khánh, P. Tân Phước Khánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0046338,
   "Latitude": 106.7199619
 },
 {
   "STT": 58,
   "Name": "Phòng Khám Đông Y Mai Thọ Đường",
   "address": "Chợ Tân Phước Khánh, P. Tân Phước Khánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0046338,
   "Latitude": 106.7199619
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc Mỹ Duyên",
   "address": "13 Lô A,Chợ Tân Phước Khánh, P. Tân Phước Khánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0046338,
   "Latitude": 106.7199619
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "Ấp Phú Thọ, xã Phú Chánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2945741,
   "Latitude": 106.7954303
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Mỹ Linh",
   "address": "KP Khánh Thạnh, P. Tân Phước Khánh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.0048707,
   "Latitude": 106.7190782
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Huê Linh Đường",
   "address": "64 KP2, TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2963709,
   "Latitude": 106.8060388
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Hạnh Hòa Đường",
   "address": "11 Độc Lập, KP2, TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2884956,
   "Latitude": 106.7930769
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "Tổ 3 Độc Lập, TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2884965,
   "Latitude": 106.7928707
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Phước Hòa",
   "address": "435A/Tổ 5, Ấp 1B, xã Phước Hòa, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.229034,
   "Latitude": 106.7302046
 },
 {
   "STT": 66,
   "Name": "Nhà thuốc Huy Mai",
   "address": "58 Độc Lập, khu phố 2, Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2897959,
   "Latitude": 106.7920768
 },
 {
   "STT": 67,
   "Name": "Nhà thuốc Lộc Thành",
   "address": "Khu phố 2 –  TT Phước Vĩnh, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2963709,
   "Latitude": 106.8060388
 },
 {
   "STT": 68,
   "Name": "Quầy thuốc Phước Hòa",
   "address": "435a ẤP 1b XÃ Phước hòa, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.229034,
   "Latitude": 106.7302046
 },
 {
   "STT": 69,
   "Name": "Nhà thuốc Ngọc Diệp",
   "address": "Xã An Ninh, Phú Giáo, huyện Phú Giáo, tỉnh Bình Dương",
   "Longtitude": 11.2963709,
   "Latitude": 106.8060388
 },
 {
   "STT": 70,
   "Name": "Quầy thuốc Đức Tuấn",
   "address": "KP1, P. Uyên Hưng, TT Tân Uyên, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0564039,
   "Latitude": 106.7910064
 },
 {
   "STT": 71,
   "Name": "Nhà thuốc Thành Hiền",
   "address": "Âp 1, xã Hội Nghĩa, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.1138462,
   "Latitude": 106.7550519
 },
 {
   "STT": 72,
   "Name": "Nhà thuốc Kim Oanh",
   "address": "Kios 14 Chợ Tân Uyên, P. Uyên Hưng, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0591162,
   "Latitude": 106.7943209
 },
 {
   "STT": 73,
   "Name": "Nhà thuốc Ngọc Nga",
   "address": "Chợ Tân Phước Khánh, P. Tân Phước Khánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0026673,
   "Latitude": 106.7322637
 },
 {
   "STT": 74,
   "Name": "Phòng Khám Đông Y Mai Thọ Đường",
   "address": "Chợ Tân Phước Khánh, P. Tân Phước Khánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0026673,
   "Latitude": 106.7322637
 },
 {
   "STT": 75,
   "Name": "Nhà thuốc Thanh Hương",
   "address": "Ấp Phú Thọ, xã Phú Chánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0675048,
   "Latitude": 106.6936799
 },
 // {
 //   "STT": 76,
 //   "Name": "Nhà thuốc Phan Phúc",
 //   "address": "3/70 Tân Phú, P. Tân Bình, thị xã Tân Uyên, tỉnh Bình Dương",
 //   "Longtitude": 10.7900517,
 //   "Latitude": 106.6281901
 // },
 {
   "STT": 77,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "22/22 KP Thống Nhất, P. Dĩ An, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.8968569,
   "Latitude": 106.7465651
 },
 {
   "STT": 78,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Âp Đông Chiêu, P. Tân Đông Hiệp, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.9184319,
   "Latitude": 106.7684285
 },
 {
   "STT": 79,
   "Name": "Nhà thuốc Thanh Hoa",
   "address": "Tân Đông Hiệp, P. Tân Đông Hiệp, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.9194771,
   "Latitude": 106.760057
 },
 {
   "STT": 80,
   "Name": "Quầy thuốc Nhật Cường",
   "address": "KP Bình Đường 4, P. An Bình, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.050326,
   "Latitude": 106.7570264
 },
 {
   "STT": 81,
   "Name": "Nhà thuốc Ngọc Hiền",
   "address": "G4 Bình Đường 2, P. An Bình, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0610906,
   "Latitude": 106.7988447
 },
 {
   "STT": 82,
   "Name": "Nhà thuốc Hạnh Phúc",
   "address": "278C Trần Hưng Đạo, P. Đông Hòa, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.8986323,
   "Latitude": 106.7815217
 },
 {
   "STT": 83,
   "Name": "Nhà thuốc Khôi Nguyên",
   "address": "413 Quốc lộ 1K, KP. Nội Hóa 1, P. Bình An, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.8989812,
   "Latitude": 106.7920599
 },
 {
   "STT": 84,
   "Name": "Nhà thuốc Tự Do",
   "address": "154 Truông Tre, KP. Bình Minh 2, P. An Bình, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.8912462,
   "Latitude": 106.767882
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "7B/10 Cô Bắc, P. Dĩ An, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.9074154,
   "Latitude": 106.7697422
 },
 {
   "STT": 87,
   "Name": "Nhà thuốc Huy Mai",
   "address": "58 Độc Lập, khu phố 2, Phước Vĩnh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.9382503,
   "Latitude": 106.7457959
 },
 {
   "STT": 88,
   "Name": "Nhà thuốc Dũng Hạnh",
   "address": "Số 4, Kp Khánh Lợi, TT Tân Phước Khánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0026673,
   "Latitude": 106.7322637
 },
 {
   "STT": 89,
   "Name": "Nhà thuốc Thùy dương",
   "address": "Kiot Số 2 Chợ Tân Lương, Thạnh Phước, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.004642,
   "Latitude": 106.7675001
 },
 {
   "STT": 90,
   "Name": "Nhà thuốc 199",
   "address": "Khu 3 Chợ Mới , Tân Uyên, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0761756,
   "Latitude": 106.691947
 },
 {
   "STT": 91,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "ấp Phú Thọ, xã Phú Chánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.0675048,
   "Latitude": 106.6936799
 },
 {
   "STT": 92,
   "Name": "Nhà thuốc Phương Anh",
   "address": "746 thị trấn Tân Thành, huỵện Bắc Tân Uyên, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.1502769,
   "Latitude": 106.8442941
 },
 {
   "STT": 93,
   "Name": "Nhà thuốc Mỹ Hằng",
   "address": "Đường DH 402 Tân Phước Khánh, thị xã Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 10.9913008,
   "Latitude": 106.7365122
 },
 // {
 //   "STT": 94,
 //   "Name": "Nhà thuốc Phan Phúc",
 //   "address": "3/70 Tân Phú, P. Tân Bình, thị xã Dĩ An, tỉnh Bình Dương",
 //   "Longtitude": 10.7900517,
 //   "Latitude": 106.6281901
 // },
 {
   "STT": 95,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "22/22 KP Thống Nhất, P. Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8968569,
   "Latitude": 106.7465651
 },
 {
   "STT": 96,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Âp Đông Chiêu, P. Tân Đông Hiệp, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9184319,
   "Latitude": 106.7684285
 },
 {
   "STT": 97,
   "Name": "Quầy thuốc Nhật Cường",
   "address": "KP Bình Đường 4, P. An Bình, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8843864,
   "Latitude": 106.7614795
 },
 {
   "STT": 98,
   "Name": "Quầy thuốc Số 9",
   "address": "54 Cô Giang, P. Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9065409,
   "Latitude": 106.7705951
 },
 {
   "STT": 99,
   "Name": "Nhà thuốc Tùng Vân",
   "address": "08 Cô Giang, Chợ Dĩ An, P. Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9072381,
   "Latitude": 106.7693307
 },
 {
   "STT": 100,
   "Name": "Nhà thuốc Bảo Minh",
   "address": "153/11A Nguyễn Thị Khắp, Chiêu Liêu, P. Tân Đông Hiệp, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9248715,
   "Latitude": 106.7497057
 },
 {
   "STT": 101,
   "Name": "Nhà thuốc Ngọc Hiền",
   "address": "G4 Bình Đường 2, P. An Bình, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8726129,
   "Latitude": 106.7582786
 },
 {
   "STT": 102,
   "Name": "Nhà thuốc Tự Do",
   "address": "154 Truông Tre, KP. Bình Minh 2, P. An Bình, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8912462,
   "Latitude": 106.767882
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Kiều Châu",
   "address": "78/12 khu phố Tây A, Đông Hòa, P. Tân Đông Hiệp, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9205051,
   "Latitude": 106.763273
 },
 {
   "STT": 105,
   "Name": "Nhà thuốc Tuấn Anh",
   "address": "17 Nguyễn Xiển, KP Quyết Thắng, P. Bình Thắng, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.897543,
   "Latitude": 106.8314543
 },
 {
   "STT": 106,
   "Name": "Nhà thuốc Hạnh Phúc",
   "address": "278C Trần Hưng Đạo, P. Đông Hòa, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8986323,
   "Latitude": 106.7815217
 },
 {
   "STT": 107,
   "Name": "Nhà thuốc Như Ý",
   "address": "KP. Tân Hiệp, P. Tân Bình, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9422122,
   "Latitude": 106.7553371
 },
 {
   "STT": 108,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "22/22 Kp Thống Nhất, Phường Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9033306,
   "Latitude": 106.7515723
 },
 {
   "STT": 109,
   "Name": "Nhà thuốc Số 9",
   "address": "54 Cô Giang, P. Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9062812,
   "Latitude": 106.7710397
 },
 {
   "STT": 110,
   "Name": "Nhà thuốc Tùng Vân",
   "address": "Số 8 Cô Giang, Phường Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9071024,
   "Latitude": 106.7694121
 },
 {
   "STT": 111,
   "Name": "Nhà thuốc Hiền Hạnh",
   "address": "18/10 Nguyễn Xiển, P.Ngãi Thắng, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8979427,
   "Latitude": 106.8311667
 },
 // {
 //   "STT": 112,
 //   "Name": "Nhà thuốc Hồng Phúc",
 //   "address": "3/70 Tân Phú, Tân Bình, thị xã Dĩ An, tỉnh Bình Dương",
 //   "Longtitude": 10.7900517,
 //   "Latitude": 106.6281901
 // },
 {
   "STT": 113,
   "Name": "Nhà thuốc Tuấn Anh 1",
   "address": "192B/5 KP Bình Đường 3, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8722598,
   "Latitude": 106.7546882
 },
 {
   "STT": 114,
   "Name": "Nhà thuốc Hạnh Phúc",
   "address": "278c Trần Hưng Đạo, phường Đông hòa, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9011166,
   "Latitude": 106.776568
 },
 {
   "STT": 115,
   "Name": "Nhà thuốc Bảo Minh",
   "address": "Chiêu Liêu, Tân Đông Hiệp, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.919634,
   "Latitude": 106.7599815
 },
 {
   "STT": 116,
   "Name": "Nhà thuốc Bình Đường",
   "address": "H1 đường 2, P. An Bình, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8842618,
   "Latitude": 106.7571551
 },
 {
   "STT": 117,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "22/22kp Thống Nhất, P. Dĩ An, TX Dĩ An, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8968569,
   "Latitude": 106.7465651
 },
 {
   "STT": 118,
   "Name": "Quầy thuốc Danh Cường",
   "address": "110/2A KDC Đông An, P. Tân Đông Hiệp, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.9268839,
   "Latitude": 106.7426729
 },
 {
   "STT": 119,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Số 9 Nguyễn Xiển, P. Bình Thắng, thị xã Dĩ An, tỉnh Bình Dương",
   "Longtitude": 10.8978221,
   "Latitude": 106.8313417
 },
 {
   "STT": 120,
   "Name": "Nhà thuốc Tân Hoàng Vinh",
   "address": "16G/3 Bình Đáng, P. Bình Hòa, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9175441,
   "Latitude": 106.734176
 },
 {
   "STT": 121,
   "Name": "Nhà thuốc My Châu 5",
   "address": "2/148 Thủ Khoa Huân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9559934,
   "Latitude": 106.7018259
 },
 {
   "STT": 122,
   "Name": "Nhà thuốc Ngọc Hiếu",
   "address": "5/9 Thủ Khoa Huân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9558831,
   "Latitude": 106.7020699
 },
 {
   "STT": 123,
   "Name": "Nhà thuốc An Khang",
   "address": "235/1A Thủ Khoa Huân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9558831,
   "Latitude": 106.7020699
 },
 {
   "STT": 124,
   "Name": "Nhà thuốc Bảo Liên",
   "address": "1/18 Ấp Hoà Lân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9558058,
   "Latitude": 106.7019673
 },
 {
   "STT": 125,
   "Name": "Nhà thuốc Thiên Phúc",
   "address": "333 KP Hoà Lân 2, P Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9504092,
   "Latitude": 106.7068242
 },
 {
   "STT": 126,
   "Name": "Nhà thuốc Quang Minh",
   "address": "Khu dân cư Thuận Giao, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.96373,
   "Latitude": 106.719764
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc Ngọc Duy",
   "address": "40A Dc 09 Đường 17, Khu dân cư Vietsing, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9418326,
   "Latitude": 106.7348379
 },
 {
   "STT": 128,
   "Name": "Nhà thuốc Minh Anh",
   "address": "106/2 KP 1B, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9599254,
   "Latitude": 106.7434989
 },
 {
   "STT": 129,
   "Name": "Nhà thuốc Số 198",
   "address": "1/48 KP Hòa Lân 2, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9571535,
   "Latitude": 106.7175669
 },
 {
   "STT": 130,
   "Name": "Nhà thuốc Minh Phương",
   "address": "Ấp Bình Phước A, P. Bình Chuẩn, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9741397,
   "Latitude": 106.7426491
 },
 {
   "STT": 131,
   "Name": "Quầy thuốc 558",
   "address": "DC 20, Ô 11, Khu dân cư Viet Sing, KP 4, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9418326,
   "Latitude": 106.7348379
 },
 {
   "STT": 132,
   "Name": "Nhà thuốc Minh Châu",
   "address": "C189 Đường DT745, P. Lái Thiêu, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9132585,
   "Latitude": 106.6985006
 },
 {
   "STT": 133,
   "Name": "Nhà thuốc Hoàng Long",
   "address": "Chung cư Đồng An, P. Bình Hòa, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8935673,
   "Latitude": 106.7340827
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc Anh Đức",
   "address": "C1, 004, KP. Hòa Long, P. Vĩnh Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8851502,
   "Latitude": 106.7010517
 },
 {
   "STT": 135,
   "Name": "Nhà thuốc Ngọc Hiếu",
   "address": "5/9 Thủ Khoa Huân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9558831,
   "Latitude": 106.7020699
 },
 {
   "STT": 136,
   "Name": "Nhà thuốc An Khang",
   "address": "235/1A Thủ Khoa Huân, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9558831,
   "Latitude": 106.7020699
 },
 {
   "STT": 137,
   "Name": "Nhà thuốc Quang Minh",
   "address": "Khu dân cư Thuận Giao, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.96373,
   "Latitude": 106.719764
 },
 {
   "STT": 138,
   "Name": "Quầy thuốc Ngọc Duy",
   "address": "40A Dc 09 Đường 17, Khu dân cư Vietsing, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9418326,
   "Latitude": 106.7348379
 },
 {
   "STT": 139,
   "Name": "Nhà thuốc Minh Anh",
   "address": "106/2 KP 1B, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9599254,
   "Latitude": 106.7434989
 },
 {
   "STT": 140,
   "Name": "Nhà thuốc Số 198",
   "address": "1/48 KP Hòa Lân 2, P. Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9571535,
   "Latitude": 106.7175669
 },
 {
   "STT": 141,
   "Name": "Nhà thuốc Thiên Phúc",
   "address": "333 KP Hoà Lân 2, P Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9504092,
   "Latitude": 106.7068242
 },
 {
   "STT": 142,
   "Name": "Quầy thuốc 558",
   "address": "DC 20, Ô 11, Khu dân cư Viet Sing, KP 4, P. An Phú, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9418326,
   "Latitude": 106.7348379
 },
 {
   "STT": 143,
   "Name": "Nhà thuốc Minh Phương",
   "address": "Ấp Bình Phước A, P. Bình Chuẩn, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9741397,
   "Latitude": 106.7426491
 },
 {
   "STT": 144,
   "Name": "Quầy thuốc Thành Vinh",
   "address": "Chợ Bình Đáng, P. Bình Hòa, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.906742,
   "Latitude": 106.727963
 },
 {
   "STT": 145,
   "Name": "Nhà thuốc Minh Châu",
   "address": "C189 Đường DT745, P. Lái Thiêu, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9132585,
   "Latitude": 106.6985006
 },
 {
   "STT": 146,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "Đồng An 2, P. Bình Hòa, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8981759,
   "Latitude": 106.7227976
 },
 {
   "STT": 147,
   "Name": "Nhà thuốc Minh Huy 2",
   "address": "Chung Cư Ehome, KP Phú Long, Vĩnh Phú, P. Lái Thiêu, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8857893,
   "Latitude": 106.6994333
 },
 {
   "STT": 148,
   "Name": "Nhà thuốc Hồng Quyên",
   "address": "44 KP Chợ,Đường DT 745,Lái Thiêu,Thuận An, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8965807,
   "Latitude": 106.689144
 },
 {
   "STT": 149,
   "Name": "Quầy thuốc Ngọc Duy",
   "address": "40A Dc09, đường 17 KDC VietSing, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9418326,
   "Latitude": 106.7348379
 },
 {
   "STT": 150,
   "Name": "Nhà thuốc Hồng Thái",
   "address": "Khu dân cư Thuận Giao, P. Thuận Giao Thị xã Thuận An, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.96373,
   "Latitude": 106.719764
 },
 {
   "STT": 151,
   "Name": "Nhà thuốc Bá Hải",
   "address": "30/2 Bình Quới 2, Bình Chuẩn, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9815391,
   "Latitude": 106.7096918
 },
 {
   "STT": 152,
   "Name": "Nhà thuốc Thành Vinh",
   "address": "Khu phố Bình Đáng, p.Bình Hòa, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9173304,
   "Latitude": 106.7341629
 },
 {
   "STT": 153,
   "Name": "Nhà thuốc Đức Mạnh",
   "address": "Đường d4 khu dân cư Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9065079,
   "Latitude": 106.7406991
 },
 {
   "STT": 154,
   "Name": "Nhà thuốc Ngọc Hiếu",
   "address": "5/9 Thủ Khoa Huận Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9628749,
   "Latitude": 106.7092448
 },
 {
   "STT": 155,
   "Name": "Nhà thuốc Bảo Liên",
   "address": "Ngã tư Hòa Lân – Thuận Giao, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9559934,
   "Latitude": 106.7018259
 },
 {
   "STT": 156,
   "Name": "Nhà thuốc Minh Châu 5",
   "address": "Kp chợ P. Lái Thiêu, Tx Thuận An, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9031257,
   "Latitude": 106.6995118
 },
 {
   "STT": 157,
   "Name": "Nhà thuốc Anh Đức",
   "address": "C1_004 khu chung cư Ehome P.Vĩnh Phú TX Thuận An, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.8857893,
   "Latitude": 106.6994333
 },
 {
   "STT": 158,
   "Name": "Nhà thuốc Thăng Long",
   "address": "14G3/4C khu chung cư 434 kp. Bình Đáng, P. Bình Hòa, Tx Thuận An, thị xã Thuận An, tỉnh Bình Dương",
   "Longtitude": 10.9173304,
   "Latitude": 106.7341629
 },
 {
   "STT": 159,
   "Name": "Quầy thuốc Phương Uyên",
   "address": "999 Tổ 17, Ấp 3, xã Tân Định, huyện Bắc Tân Uyên, tỉnh Bình Dương",
   "Longtitude": 11.2047519,
   "Latitude": 106.8768415
 }
];